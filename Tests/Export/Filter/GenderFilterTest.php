<?php
/*
 * Copyright (C) 2016 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Tests\Export\Filter;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class GenderFilterTest extends AbstractFilterTest
{
    /**
     *
     * @var \Chill\PersonBundle\Export\Filter\GenderFilter
     */
    private $filter;
    
    public function setUp()
    {
        static::bootKernel();
        
        $container = static::$kernel->getContainer();
        
        $this->filter = $container->get('chill.person.export.filter_gender');   
    }
    
    
    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        return array(
            array(
                'accepted_genders' => Person::FEMALE_GENDER
            ),
            array(
                'accepted_genders' => Person::MALE_GENDER
            )
        );
    }

    public function getQueryBuilders()
    {
        if (static::$kernel === null) {
            static::bootKernel();
        }
        
        $em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
        
        return array(
            $em->createQueryBuilder()
                ->select('p.firstName')
                ->from('ChillPersonBundle:Person', 'p')
        );
    }
}
