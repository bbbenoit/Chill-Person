<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Tests\Export\Filter;

use Chill\MainBundle\Test\Export\AbstractFilterTest;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BirthdayFilterTest extends AbstractFilterTest
{
    /**
     *
     * @var \Chill\PersonBundle\Export\Filter\BirthdateFilter
     */
    private $filter;
    
    public function setUp()
    {
        static::bootKernel();
        
        $container = static::$kernel->getContainer();
        
        $this->filter = $container->get('chill.person.export.filter_birthdate');   
    }
    
    
    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        return array(
            array(
                'date_from' => \DateTime::createFromFormat('Y-m-d', '2000-01-01'),
                'date_to'   => \DateTime::createFromFormat('Y-m-d', '2010-01-01')
            )
        );
    }

    public function getQueryBuilders()
    {
        if (static::$kernel === null) {
            static::bootKernel();
        }
        
        $em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
        
        return array(
            $em->createQueryBuilder()
                ->select('p.firstName')
                ->from('ChillPersonBundle:Person', 'p'),
            $em->createQueryBuilder()
                ->select('p.firstName')
                ->from('ChillPersonBundle:Person', 'p')
                // add a dummy where clause
                ->where('p.firstname IS NOT NULL'),
            $em->createQueryBuilder()
                ->select('count(IDENTITY(p))')
                ->from('ChillPersonBundle:Person', 'p')
                // add a dummy where clause
                ->where('p.firstname IS NOT NULL')
        );
    }
}
