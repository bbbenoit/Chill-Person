<?php
/*
 * Copyright (C) 2016 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;

/**
 * Test CountPerson export
 *
 * @author julien.fastre@champs-libres.coop
 */
class CountPersonTest extends AbstractExportTest
{
    /**
     *
     * @var 
     */
    private $export;
    
    public function setUp()
    {
        static::bootKernel();
        
        /* @var $container \Symfony\Component\DependencyInjection\ContainerInterface */
        $container = self::$kernel->getContainer();
        
        $this->export = $container->get('chill.person.export.export_count_person');
    }
    
    
    public function getExport()
    {
        return $this->export;
    }

    public function getFormData()
    {
        return array(
            array()
        );
    }

    public function getModifiersCombination()
    {
        return array( [ 'person' ] );
    }
}
