<?php
/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Export\ListPerson;


/**
 * Test the export "ListPerson"
 *
 * @author julien.fastre@champs-libres.coop
 */
class ListPersonTest extends AbstractExportTest
{
    /**
     *
     * @var ListPerson 
     */
    private $export;
    
    
    public function setUp()
    {
            static::bootKernel();
        
        /* @var $container \Symfony\Component\DependencyInjection\ContainerInterface */
        $container = self::$kernel->getContainer();
        
        $this->export = $container->get('chill.person.export.list_person');
        
        // add a fake request with a default locale (used in translatable string)
        $prophet = new \Prophecy\Prophet;
        $request = $prophet->prophesize();
        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');
        
        $container->get('request_stack')
                ->push($request->reveal());
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getExport()
    {
        return $this->export;
    }

    public function getFormData()
    {
        return array(
            array('fields' => ['id', 'firstName', 'lastName']),
            array('fields' => ['id', 'birthdate', 'gender', 'memo', 'email', 'phonenumber']),
            array('fields' => ['firstName', 'lastName', 'phonenumber']),
            array('fields' => ['id', 'nationality']),
            array('fields' => ['id', 'countryOfBirth']),
            array('fields' => ['id', 'address_street_address_1',
                'address_street_address_2', 'address_valid_from', 
                'address_postcode_label', 'address_postcode_code', 
                'address_country_name', 'address_country_code'],
                  'address_date' => \DateTime::createFromFormat('Y-m-d', '2016-06-12'))
        );
    }
    
    public function getModifiersCombination()
    {
        return array(
            array('person')
        );
    }
    
    
}
