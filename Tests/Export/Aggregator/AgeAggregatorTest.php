<?php

/*
 * Copyright (C) 2017 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AgeAggregatorTest extends AbstractAggregatorTest
{
    /**
     *
     * @var \Chill\PersonBundle\Export\Aggregator\AgeAggregator
     */
    private $aggregator;
    
    public function setUp()
    {
        static::bootKernel();
        
        $container = static::$kernel->getContainer();
        
        $this->aggregator = $container->get('chill.person.export.aggregator_age');   
    }
    
    public function getAggregator()
    {
        return $this->aggregator;
    }
    
    public function getFormData()
    {
        return array(
            array(
                'date_age_calculation' => \DateTime::createFromFormat('Y-m-d','2016-06-16')
                )
        );
    }
    
    public function getQueryBuilders()
    {
        if (static::$kernel === null) {
            static::bootKernel();
        }
        
        $em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
        
        return array(
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from('ChillPersonBundle:Person', 'person')
        );
    }

}
