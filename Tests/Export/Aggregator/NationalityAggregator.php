<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Tests\Export\Aggregator;

require_once '/home/julien/dev/chill-dev/vendor/chill-project/main/Test/Export/AbstractAggregatorTest.php';
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class NationalityAggregator extends AbstractAggregatorTest
{
    /**
     *
     * @var \Chill\PersonBundle\Export\Aggregator\NationalityAggregator
     */
    private $aggregator;
    
    public function setUp()
    {
        static::bootKernel();
        
        $container = static::$kernel->getContainer();
        
        $this->aggregator = $container->get('chill.person.export.aggregator_nationality');   
    }
    
    public function getAggregator()
    {
        return $this->aggregator;
    }
    
    public function getFormData()
    {
        return array(
            array('group_by_level' => 'country'),
            array('group_by_level' => 'continent')
        );
    }
    
    public function getQueryBuilders()
    {
        if (static::$kernel === null) {
            static::bootKernel();
        }
        
        $em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
        
        return array(
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from('ChillPersonBundle:Person', 'person')
        );
    }
}
