<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2015, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Tests\Entity;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;

class AccompanyingPeriodTest extends \PHPUnit_Framework_TestCase
{

    public function testClosingIsAfterOpeningConsistency()
    {
        $datetime1 = new \DateTime('now');
        $datetime2 = new \DateTime('tomorrow');

        $period = new AccompanyingPeriod($datetime1);
        $period->setClosingDate($datetime2);

        $r = $period->isClosingAfterOpening();

        $this->assertTrue($r);
    }

    public function testClosingIsBeforeOpeningConsistency()
    {
        $datetime1 = new \DateTime('tomorrow');
        $datetime2 = new \DateTime('now');

        $period = new AccompanyingPeriod($datetime1);
        $period->setClosingDate($datetime2);

        $this->assertFalse($period->isClosingAfterOpening());
    }

    public function testClosingEqualOpening()
    {
        $datetime = new \DateTime('now');

        $period = new AccompanyingPeriod($datetime);
        $period->setClosingDate($datetime);

        $this->assertTrue($period->isClosingAfterOpening());
    }

    public function testIsOpen()
    {
        $period = new AccompanyingPeriod(new \DateTime());

        $this->assertTrue($period->isOpen());
    }

    public function testIsClosed()
    {
        $period = new AccompanyingPeriod(new \DateTime());
        $period->setClosingDate(new \DateTime('tomorrow'));

        $this->assertFalse($period->isOpen());
    }

    public function testCanBeReOpened()
    {
        $person = new Person(\DateTime::createFromFormat('Y-m-d', '2010-01-01'));
        $person->close($person->getAccompanyingPeriods()[0]
                ->setClosingDate(\DateTime::createFromFormat('Y-m-d', '2010-12-31')));
        
        $firstAccompanygingPeriod = $person->getAccompanyingPeriodsOrdered()[0];
        
        $this->assertTrue($firstAccompanygingPeriod->canBeReOpened());
        
        $lastAccompanyingPeriod = (new AccompanyingPeriod(\DateTime::createFromFormat('Y-m-d', '2011-01-01')))
                ->setClosingDate(\DateTime::createFromFormat('Y-m-d', '2011-12-31'))
                ;
        $person->addAccompanyingPeriod($lastAccompanyingPeriod);
        
        $this->assertFalse($firstAccompanygingPeriod->canBeReOpened());
    }

}
