<?php

namespace Chill\PersonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Chill\PersonBundle\Widget\PersonListWidgetFactory;

class ChillPersonBundle extends Bundle
{
    public function build(ContainerBuilder $container) 
    {
        parent::build($container);
        
        $container->getExtension('chill_main')
            ->addWidgetFactory(new PersonListWidgetFactory());
    }
}
