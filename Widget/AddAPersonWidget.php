<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Chill\PersonBundle\Widget;

use Chill\MainBundle\Templating\Widget\WidgetInterface;


/**
 * Add a button "add a person"
 *
 * @author julien
 */
class AddAPersonWidget implements WidgetInterface
{
    public function render(
            \Twig_Environment $env, 
            $place, 
            array $context, 
            array $config) {
        return $env->render("ChillPersonBundle:Widget:homepage_add_a_person.html.twig");
   }
}
