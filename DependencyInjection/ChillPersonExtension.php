<?php

/*
 * Copyright (C) 2014-2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\MainBundle\DependencyInjection\MissingBundleException;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\PersonBundle\Doctrine\DQL\AddressPart;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillPersonExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        // set configuration for double metaphone
        $container->setParameter('cl_chill_person.search.use_double_metaphone', 
                $config['search']['use_double_metaphone']);
        
        // set configuration for validation
        $container->setParameter('chill_person.validation.birtdate_not_before',
                $config['validation']['birthdate_not_after']);
        
        $this->handlePersonFieldsParameters($container, $config['person_fields']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/widgets.yml');
        $loader->load('services/exports.yml');
    }
    
    private function handlePersonFieldsParameters(ContainerBuilder $container, $config)
    {
        if (array_key_exists('enabled', $config)) {
            unset($config['enabled']);
        }
        
        $container->setParameter('chill_person.person_fields', $config);
        
        foreach ($config as $key => $value) {
            $container->setParameter('chill_person.person_fields.'.$key, $value);
        }
    }

    private function declarePersonAsCustomizable (ContainerBuilder $container)
    {
        $bundles = $container->getParameter('kernel.bundles');
        if (!isset($bundles['ChillCustomFieldsBundle'])) {
            throw new MissingBundleException('ChillCustomFieldsBundle');
        }

        $container->prependExtensionConfig('chill_custom_fields',
            array('customizables_entities' => 
                array(
                    array('class' => 'Chill\PersonBundle\Entity\Person', 'name' => 'PersonEntity')
                )
            )
        );
    }
    
    public function prepend(ContainerBuilder $container) 
    {
        $this->prependRoleHierarchy($container);
        $this->prependHomepageWidget($container);
        $this->prependDoctrineDQL($container);
                
        $bundles = $container->getParameter('kernel.bundles');
        //add ChillMain to assetic-enabled bundles
        if (!isset($bundles['AsseticBundle'])) {
            throw new MissingBundleException('AsseticBundle');
        }

        $asseticConfig = $container->getExtensionConfig('assetic');
        $asseticConfig['bundles'][] = 'ChillPersonBundle';
        $container->prependExtensionConfig('assetic', 
                array('bundles' => array('ChillPersonBundle')));
        
        //add person_fields parameter as global
        $chillPersonConfig = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $chillPersonConfig);
        $twigConfig = array(
            'globals' => array(
                'chill_person' => array(
                    'fields' => $config['person_fields']
                )
            ),
            'form_themes' => array('ChillPersonBundle:Export:ListPersonFormFields.html.twig')
        );
        $container->prependExtensionConfig('twig', $twigConfig);

        $this-> declarePersonAsCustomizable($container);
        
        //declare routes for person bundle
         $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillPersonBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    /**
     * 
     * Add a widget "add a person" on the homepage, automatically
     * 
     * @param \Chill\PersonBundle\DependencyInjection\containerBuilder $container
     */
    protected function prependHomepageWidget(containerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', array(
            'widgets' => array(
                'homepage' => array(
                    array(
                        'widget_alias' => 'add_person',
                        'order' => 2
                    )
                )
            )
        ));
    }
    
    /**
     * Add role hierarchy.
     * 
     * @param ContainerBuilder $container
     */
    protected function prependRoleHierarchy(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
              'CHILL_PERSON_UPDATE' => array('CHILL_PERSON_SEE'),
              'CHILL_PERSON_CREATE' => array('CHILL_PERSON_SEE'),
              'CHILL_PERSON_SEE'    => array(PersonVoter::STATS)
           )
        ));
    }
    
    /**
     * Add DQL function linked with person
     * 
     * @param ContainerBuilder $container
     */
    protected function prependDoctrineDQL(ContainerBuilder $container)
    {
        //add DQL function to ORM (default entity_manager)
        
        $container->prependExtensionConfig('doctrine', array(
           'orm' => array(
              'dql' => array(
                 'string_functions' => array(
                    'GET_PERSON_ADDRESS_ADDRESS_ID' => AddressPart\AddressPartAddressId::class,
                    'GET_PERSON_ADDRESS_STREET_ADDRESS_1' => AddressPart\AddressPartStreetAddress1::class,
                    'GET_PERSON_ADDRESS_STREET_ADDRESS_2' => AddressPart\AddressPartStreetAddress2::class,
                    'GET_PERSON_ADDRESS_VALID_FROM' => AddressPart\AddressPartValidFrom::class,
                    'GET_PERSON_ADDRESS_POSTCODE_LABEL' => AddressPart\AddressPartPostCodeLabel::class,
                    'GET_PERSON_ADDRESS_POSTCODE_CODE' => AddressPart\AddressPartPostCodeCode::class,
                    'GET_PERSON_ADDRESS_POSTCODE_ID'   => AddressPart\AddressPartPostCodeId::class,
                    'GET_PERSON_ADDRESS_COUNTRY_NAME'  => AddressPart\AddressPartCountryName::class,
                    'GET_PERSON_ADDRESS_COUNTRY_CODE'  => AddressPart\AddressPartCountryCode::class,
                    'GET_PERSON_ADDRESS_COUNTRY_ID'    => AddressPart\AddressPartCountryId::class
                 )
              )
           )
        ));
    }
}
