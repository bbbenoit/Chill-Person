<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Export\Aggregator;

use Chill\MainBundle\Export\AggregatorInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Translation\TranslatorInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Declarations;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class GenderAggregator implements AggregatorInterface
{
   
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    public function applyOn()
    {
        return Declarations::PERSON_TYPE;
    }
    
    
    public function buildForm(FormBuilderInterface $builder)
    {
        
    }
    
    public function alterQuery(QueryBuilder $qb, $data)
    {

        $qb->addSelect('person.gender as gender');
        
        $qb->addGroupBy('gender');
        
    }
    
    public function getTitle()
    {
        return "Group people by gender";
    }
    
    public function getQueryKeys($data)
    {
        return array('gender');
    }
    
    public function getLabels($key, array $values, $data)
    {    
        return function($value) {
            switch ($value) {
                case Person::FEMALE_GENDER : 
                    return $this->translator->trans('woman');
                case Person::MALE_GENDER   : 
                    return $this->translator->trans('man');
                case '_header' : 
                    return $this->translator->trans('Gender');
                default:
                    throw new \LogicException(sprintf("The value %s is not valid", $value));
            }
        };
    }

    public function addRole()
    {
        return NULL;
    }

}
