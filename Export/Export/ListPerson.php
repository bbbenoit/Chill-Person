<?php

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\ListInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\Query;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;
use Chill\PersonBundle\Export\Declarations;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Chill\MainBundle\Export\ExportElementValidatedInterface;

/**
 * Render a list of peoples
 *
 * @author julien
 */
class ListPerson implements ListInterface, ExportElementValidatedInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     * 
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var CustomFieldProvider
     */
    protected $customFieldProvider;
    
    protected $fields = array(
        'id',  'firstName', 'lastName', 'birthdate',
        'placeOfBirth', 'gender', 'memo', 'email', 'phonenumber',
        'countryOfBirth', 'nationality', 'address_street_address_1',
        'address_street_address_2', 'address_valid_from', 'address_postcode_label',
        'address_postcode_code', 'address_country_name', 'address_country_code'
    );
    
    public function __construct(
            EntityManagerInterface $em,
            TranslatorInterface $translator,
            TranslatableStringHelper $translatableStringHelper,
            CustomFieldProvider $customFieldProvider
    ) {
        $this->entityManager = $em;
        $this->translator = $translator;
        $this->translatableStringHelper = $translatableStringHelper;
        $this->customFieldProvider = $customFieldProvider;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @param FormBuilderInterface $builder
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $choices = array_combine($this->fields, $this->fields);
        
        foreach ($this->getCustomFields() as $cf) {
            $choices
                [$this->translatableStringHelper->localize($cf->getName())] 
                = 
                $cf->getSlug();
        }
        
        // Add a checkbox to select fields
        $builder->add('fields', ChoiceType::class, array(
            'multiple' => true,
            'expanded' => true,
            'choices' => $choices,
            'choices_as_values' => true,
            'label'    => 'Fields to include in export',
            'choice_attr' => function($val, $key, $index) {
                // add a 'data-display-target' for address fields
                if (substr($val, 0, 8) === 'address_') {
                    return ['data-display-target' => 'address_date'];
                } else {
                    return [];
                }
            },
            'constraints' => [new Callback(array(
                'callback' => function($selected, ExecutionContextInterface $context) {
                    if (count($selected) === 0) {
                        $context->buildViolation('You must select at least one element')
                            ->atPath('fields')
                            ->addViolation();
                    }
                }
            ))]
        ));
        
        // add a date  field for addresses
        $builder->add('address_date', DateType::class, array(
            'label' => "Address valid at this date",
            'data'  => new \DateTime(),
            'attr'  => array( 'class' => 'datepicker'),
            'widget'=> 'single_text',
            'format' => 'dd-MM-yyyy',
            'required' => false,
            'block_name' => 'list_export_form_address_date'
        ));
    }
    
    public function validateForm($data, ExecutionContextInterface $context)
    {
        dump($data);
        // get the field starting with address_
        $addressFields = array_filter(function($el) {
                return substr($el, 0, 8) === 'address_';
            }, $this->fields);
        dump($data['address_date']);
        // check if there is one field starting with address in data
        if (count(array_intersect($data['fields'], $addressFields)) > 0) {
            // if a field address is checked, the date must not be empty
            if (empty($data['address_date'])) {
                $context
                    ->buildViolation("You must set this date if an address is checked")
                    ->atPath('address_date')
                    ->addViolation();
            }
        }
    }
    
    /**
     * Get custom fields associated with person
     * 
     * @return CustomField[]
     */
    private function getCustomFields()
    {
        return $this->entityManager
            ->createQuery("SELECT cf "
                . "FROM ChillCustomFieldsBundle:CustomField cf "
                . "JOIN cf.customFieldGroup g "
                . "WHERE cf.type != :title AND g.entity LIKE :entity")
            ->setParameters(array(
                'title'  => 'title',
                'entity' => \addcslashes(Person::class, "\\")
            ))
            ->getResult();
    }

    /**
     * {@inheritDoc}
     * 
     * @return type
     */
    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    /**
     * {@inheritDoc}
     * 
     * @return string
     */
    public function getDescription()
    {
        return "Create a list of people according to various filters.";
    }

    /**
     * {@inheritDoc}
     * 
     * @param type $key
     * @param array $values
     * @param type $data
     * @return type
     */
    public function getLabels($key, array $values, $data)
    {
        switch ($key) {
            case 'birthdate':
                // for birthdate, we have to transform the string into a date
                // to format the date correctly.
                return function($value) {
                    if ($value === '_header') { return 'birthdate'; }
                    
                    if (empty($value))
                    {
                        return "";
                    }
                    
                    $date = \DateTime::createFromFormat('Y-m-d', $value);
                    // check that the creation could occurs.
                    if ($date === false) { 
                        throw new \Exception(sprintf("The value %s could "
                            . "not be converted to %s", $value, \DateTime::class));
                    }
                    
                    return $date->format('d-m-Y'); 
                };
            case 'gender' :
                // for gender, we have to translate men/women statement
                return function($value) {
                    if ($value === '_header') { return 'gender'; }
                    
                    return $this->translator->trans($value);
                };
            case 'countryOfBirth':
            case 'nationality':
                $countryRepository = $this->entityManager
                    ->getRepository('ChillMainBundle:Country');
                
                // load all countries in a single query
                 $countryRepository->findBy(array('countryCode' => $values));
                 
                return function($value) use ($key, $countryRepository) {
                    if ($value === '_header') { return \strtolower($key); }
                    
                    if ($value === NULL) { 
                        return $this->translator->trans('no data');
                    }
                    
                    $country = $countryRepository->find($value);
                    
                    return $this->translatableStringHelper->localize(
                            $country->getName());
                };
            case 'address_country_name':
                return function($value) use ($key) {
                    if ($value === '_header') { return \strtolower($key); }
                    
                    if ($value === NULL) {
                        return '';
                    }
                    
                    return $this->translatableStringHelper->localize(json_decode($value, true));
                };
            default:
                // for fields which are associated with person
                if (in_array($key, $this->fields)) {
                    return function($value) use ($key) {
                        if ($value === '_header') { return \strtolower($key); } 

                        return $value; 

                        };
                } else {
                    // for fields which are custom fields
                    /* @var $cf CustomField */
                    $cf = $this->entityManager
                        ->getRepository(CustomField::class)
                        ->findOneBy(array('slug' => $this->DQLToSlug($key)));
                    
                    return function($value) use ($cf) {
                        if ($value === '_header') {
                            return $this->translatableStringHelper->localize($cf->getName()); 
                        }
                        
                        return $this->customFieldProvider
                            ->getCustomFieldByType($cf->getType())
                            ->render(json_decode($value, true), $cf, 'csv');
                    };
                }
        }
        
    }

    /**
     * {@inheritDoc}
     * 
     * @param type $data
     * @return type
     */
    public function getQueryKeys($data)
    {
        $fields = array();
        
        foreach ($data['fields'] as $key) {
            if (in_array($key, $this->fields)) {
                $fields[] = $key;
            } else {
                // this should be a slug from custom field, we have to clean it
                $fields[] = $this->slugToDQL($key);
            }
        }
        
        return $fields;
    }
    
    /**
     * clean a slug to be usable by DQL
     * 
     * @param string $slugsanitize 
     * @return string
     */
    private function slugToDQL($slug)
    {
        return "cf____".\str_replace("-", "____", $slug);
    }
    
    private function DQLToSlug($cleanedSlug)
    {
        return \str_replace("____", "-", \substr($cleanedSlug, 6));
    }

    /**
     * {@inheritDoc}
     * 
     */
    public function getResult($query, $data)
    {
        return $query->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }

    /**
     * {@inheritDoc}
     * 
     * @return string
     */
    public function getTitle()
    {
        return "List peoples";
    }

    /**
     * {@inheritDoc}
     * 
     */
    public function getType()
    {
        return Declarations::PERSON_TYPE;
    }

    /**
     * {@inheritDoc}
     * 
     */
    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
        $centers = array_map(function($el) { return $el['center']; }, $acl);
        
        // throw an error if any fields are present
        if (!\array_key_exists('fields', $data)) {
            throw new \Doctrine\DBAL\Exception\InvalidArgumentException("any fields "
                . "have been checked");
        }
        
        $qb = $this->entityManager->createQueryBuilder();
        
        foreach ($this->fields as $f) {
            if (in_array($f, $data['fields'])) {
                switch ($f) {
                    case 'countryOfBirth':
                    case 'nationality':
                        $qb->addSelect(sprintf('IDENTITY(person.%s) as %s', $f, $f));
                        break;
                    case 'address_street_address_1':
                    case 'address_street_address_2':
                    case 'address_valid_from':
                    case 'address_postcode_label':
                    case 'address_postcode_code':
                    case 'address_country_name':
                    case 'address_country_code':

                        $qb->addSelect(sprintf(
                            'GET_PERSON_ADDRESS_%s(person.id, :address_date) AS %s',
                            // get the part after address_
                            strtoupper(substr($f, 8)), 
                            $f));
                        $qb->setParameter('address_date', $data['address_date']);
                        break;
                    default:
                        $qb->addSelect(sprintf('person.%s as %s', $f, $f));
                }
            }
        }
        
        foreach ($this->getCustomFields() as $cf) {
            if (in_array($cf->getSlug(), $data['fields'])) {
                $slug = $this->slugToDQL($cf->getSlug());
                $qb->addSelect(
                    sprintf('GET_JSON_FIELD_BY_KEY(person.cFData, :slug%s) AS %s', 
                        $slug, $slug));
                $qb->setParameter(sprintf('slug%s', $slug), $cf->getSlug());
                //$qb->setParameter(sprintf('name%s', $slug), $cf->getSlug());
            }
        }
        
        $qb
                ->from('ChillPersonBundle:Person', 'person')
                ->join('person.center', 'center')
                ->andWhere('center IN (:authorized_centers)')
                ->setParameter('authorized_centers', $centers);
                ;
        
        
        return $qb;
    }

    /**
     * 
     * {@inheritDoc}
     */
    public function requiredRole()
    {
        return new Role(PersonVoter::STATS);
    }

    /**
     * 
     * {@inheritDoc}
     */
    public function supportsModifiers()
    {
        return array(Declarations::PERSON_TYPE, Declarations::PERSON_IMPLIED_IN);
    }
}
