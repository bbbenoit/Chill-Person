<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Export\Filter;

use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\QueryBuilder;
use Chill\MainBundle\Export\FilterInterface;
use Doctrine\ORM\Query\Expr;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\EntityRepository;
use Chill\MainBundle\Entity\Country;
use Chill\MainBundle\Export\ExportElementValidatedInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class NationalityFilter implements FilterInterface,
    ExportElementValidatedInterface
{
    /**
     *
     * @var TranslatableStringHelper
     */
    private $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $helper) 
    {
        $this->translatableStringHelper = $helper;
    }
    
    public function applyOn()
    {
        return 'person';
    }
    
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('nationalities', 'select2_chill_country', array(
            'placeholder' => 'Choose countries'
        ));
        
    }
    
    public function validateForm($data, ExecutionContextInterface $context)
    {
        if ($data['nationalities'] === null) {
            $context->buildViolation("A nationality must be selected")
                ->addViolation();
        }
    }
    
    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->in('person.nationality', ':person_nationality');

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('person_nationality', array($data['nationalities']));
    }
    
    public function getTitle()
    {
        return "Filter by person's nationality";
    }
    
    public function addRole()
    {
        return NULL;
    }
    
    public function describeAction($data, $format = 'string')
    {
        $countries = $data['nationalities'];
        
        $names = array_map(function(Country $c) {
            return $this->translatableStringHelper->localize($c->getName());
        }, array($countries));
        
        return array(
            "Filtered by nationality : %nationalities%", 
            array('%nationalities%' => implode(", ", $names))
            );
    }
}
