<?php

/*
 * Copyright (C) 2017 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Export\Filter;

use Chill\MainBundle\Export\FilterInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\Query\Expr;
use Chill\MainBundle\Form\Type\Export\FilterType;
use Symfony\Component\Form\FormError;
use Chill\MainBundle\Export\ExportElementValidatedInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BirthdateFilter implements FilterInterface, ExportElementValidatedInterface
{
    
    public function addRole()
    {
        return null;
    }

    public function alterQuery(\Doctrine\ORM\QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->between('person.birthdate', ':date_from', 
            ':date_to');

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }
        
        $qb->add('where', $where);
        $qb->setParameter('date_from', $data['date_from']);
        $qb->setParameter('date_to', $data['date_to']);
    }

    public function applyOn()
    {
        return 'person';
    }

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder)
    {
        $builder->add('date_from', DateType::class, array(
            'label' => "Born after this date",
            'data'  => new \DateTime(),
            'attr'  => array('class' => 'datepicker'),
            'widget'=> 'single_text',
            'format' => 'dd-MM-yyyy',
        ));
        
        $builder->add('date_to', DateType::class, array(
            'label' => "Born before this date",
            'data'  => new \DateTime(),
            'attr'  => array('class' => 'datepicker'),
            'widget'=> 'single_text',
            'format' => 'dd-MM-yyyy',
        ));
        
    }
    
    public function validateForm($data, ExecutionContextInterface $context)
    { 
        $date_from = $data['date_from'];
        $date_to   = $data['date_to'];
        dump($date_from, $date_to);
        
        if ($date_from === null) {
            $context->buildViolation('The "date from" should not be empty')
                //->atPath('date_from')
                ->addViolation();
        }
        
        if ($date_to === null) {
            $context->buildViolation('The "date to" should not be empty')
                //->atPath('date_to')
                ->addViolation();
        }
        
        if (
            ($date_from !== null && $date_to !== null)
            &&
            $date_from >= $date_to
        ) {
            $context->buildViolation('The date "date to" should be after the '
                . 'date given in "date from" field')
                ->addViolation();
        }
    }

    public function describeAction($data, $format = 'string')
    {
        return array('Filtered by person\'s birtdate: '
            . 'between %date_from% and %date_to%', array(
                '%date_from%' => $data['date_from']->format('d-m-Y'),
                '%date_to%'   => $data['date_to']->format('d-m-Y')
            ));
    }

    public function getTitle()
    {
        return 'Filter by person\'s birthdate';
    }

}
