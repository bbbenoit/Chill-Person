<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Check if each person do not have multiple addresses with the same valideFrom
 */
class Version20160422000000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $stmt = $this->connection->query('SELECT COUNT(*), pe.id, ad.validfrom FROM person AS pe
	        INNER JOIN chill_person_persons_to_addresses AS pe_ad ON pe.id = pe_ad.person_id
	        INNER JOIN chill_main_address AS ad ON ad.id = pe_ad.address_id
	        GROUP BY pe.id, ad.validfrom
	        HAVING COUNT(*) > 1');
            
        $personWithTwoAddressWithSameValidFrom = $stmt->fetchAll();
        
        foreach ($personWithTwoAddressWithSameValidFrom as $p) {
            $this->warnIf(true, 'The person with id '.$p['id'].' has two adresses with the same validFrom date');
        }
        
        $this->abortIf(
            sizeof($personWithTwoAddressWithSameValidFrom) != 0,
            'There exists some person with multiple adress with the same validFrom'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
