<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add the prefix 'chill_person' to all the (db) table name of this bundle
 */
class Version20160818151130 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE person RENAME TO chill_person_person');
        $this->addSql('ALTER TABLE person_id_seq RENAME TO chill_person_person_id_seq');

        $this->addSql('ALTER TABLE marital_status RENAME TO chill_person_marital_status');

        $this->addSql('ALTER TABLE accompanying_period RENAME TO chill_person_accompanying_period');
        $this->addSql('ALTER TABLE accompanying_period_id_seq RENAME TO chill_person_accompanying_period_id_seq');

        $this->addSql('ALTER TABLE closingmotive RENAME TO chill_person_closingmotive');
        $this->addSql('ALTER TABLE closingmotive_id_seq RENAME TO chill_person_closingmotive_id_seq');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSQL('ALTER TABLE chill_person_person RENAME TO person');
        $this->addSql('ALTER TABLE chill_person_person_id_seq RENAME TO person_id_seq');

        $this->addSQL('ALTER TABLE chill_person_marital_status RENAME TO marital_status ');

        $this->addSQl('ALTER TABLE chill_person_accompanying_period RENAME TO accompanying_period');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_id_seq RENAME TO accompanying_period_id_seq');

        $this->addSQL('ALTER TABLE chill_person_closingmotive RENAME TO closingmotive');
        $this->addSql('ALTER TABLE chill_person_closingmotive_id_seq RENAME TO closingmotive_id_seq');
    }
}
