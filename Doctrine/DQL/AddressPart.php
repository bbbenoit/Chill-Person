<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * 
 * USAGE GET_ADDRESS_<part>(person.id, :date, 'postcode') where part
 * should be replace by the part of the address.
 * 
 * This function return the current address part at the given date, for the
 * given person (identified by his id)
 * 
 * The aim of this function is to be used within reports
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
abstract class AddressPart extends FunctionNode
{
    public $fields = array(
            'address_id', 
            'streetaddress1', 
            'streetaddress2',
            'validfrom', 
            'postcode_label', 
            'postcode_code', 
            'postcode_id', 
            'country_name', 
            'country_code', 
            'country_id'
        );
    
    /**
     *
     * @var \Doctrine\ORM\Query\AST\Node
     */
    private $pid;
    
    /**
     *
     * @var \Doctrine\ORM\Query\AST\Node
     */
    private $date;
    
    /**
     *
     * @var \Doctrine\ORM\Query\AST\Node
     */
    private $part;
    
    /**
     * return the part of the address
     * 
     * Should be one value of the "public" amongst 
     * 'address_id', 'streetaddress1', 
     * 'streetaddress2', 'validfrom',  'postcode_label',  'postcode_code', 
     * 'postcode_id', 'country_name', 'country_code',  'country_id'
     * 
     * @return string
     */
    abstract public function getPart();
        
    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            'get_last_address_%s(%s, %s)', 
            $this->getPart(),
            $this->pid->dispatch($sqlWalker),
            $this->date->dispatch($sqlWalker)
            );
    }

    public function parse(Parser $parser)
    {
        $a = $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        // person id
        $this->pid = $parser->SingleValuedPathExpression();
        $parser->match(Lexer::T_COMMA);
        // date
        $this->date = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
